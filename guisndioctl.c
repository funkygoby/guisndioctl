#include <errno.h>
#include <poll.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sndio.h>

#include <gtk/gtk.h>

/* A control represents an instance of sioctl_desc. The widget is chosen depending on the type
 * Controls widgets are organized in a tree group/slice/control
 * Controls sharing the same name will be inside the same slice (level+mute)
 * Slice sharing the same group (app) will be inside the same group. If no group (input/output), the name is used and differenciation is done through node0.unit
 */

#define STR_MAXSIZE 16
struct control {
	/* Tree */
	struct slice *slice;
	struct control *sibling; /* Next control in the slice */

	/* sndio */
	struct control *next; /* Global linked list used for lookup */
	unsigned int addr;
	unsigned int type;

	GtkWidget *widget;
};

struct slice {
	struct group *group;
	struct slice *sibling;
	struct control *control;

	char name[STR_MAXSIZE]; /* Use dynamic alloc? */
	GtkWidget *vbox;
};

struct group {
	struct group *sibling;
	struct slice *slice;

	char name[STR_MAXSIZE];
	GtkWidget *hbox; /* Contains the slices */
	GtkWidget *vbox; /* Contains the label + hbox */
};

/* Contains all the data that need to be passed around */
struct gui {
	struct control *control; /* Complete linked list for addr lookup */
	struct group *group; /* Root of the graphical objects */

	/* Those are handled by the GUI, no need to _init _free */
	GtkWidget *window;
	GtkWidget *box;
};

void on_control_value_changed(GtkWidget *, gpointer);
struct sioctl_hdl *hdl;

/* Structures methods */
struct control *
control_new(struct sioctl_desc *d)
{
	struct control *c;

	c = malloc(sizeof(struct control));
	c->slice = NULL;
	c->sibling = NULL;

	c->next = NULL;
	c->addr = d->addr;
	c->type = d->type;
	switch (d->type) {
	case SIOCTL_NUM:
		c->widget = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0., (float)d->maxval, 1.); /* scale vs volume_button */
		gtk_range_set_inverted(GTK_RANGE(c->widget), TRUE);
		g_signal_connect(c->widget, "value-changed", G_CALLBACK(on_control_value_changed), c);
		break;
	case SIOCTL_SW:
		c->widget = gtk_check_button_new_with_label("Mute");
		gtk_widget_set_halign(c->widget, GTK_ALIGN_CENTER);
		g_signal_connect(c->widget, "toggled", G_CALLBACK(on_control_value_changed), c);
		break;
	case SIOCTL_LIST:
		;
	case SIOCTL_VEC:
		printf("SIOCTL_LIST and SIOCTL_VEC not supported yet in guisndioctl\n%d will be ignored", d->addr);
		break;
	}
	return c;
}

void
control_free(struct control *c)
{
	gtk_widget_destroy(c->widget);
	free(c);
}

unsigned int
control_get_value(struct control *c)
{
	unsigned int value;

	switch (c->type) {
	case SIOCTL_NUM:
		value = gtk_range_get_value(GTK_RANGE(c->widget));
		break;
	case SIOCTL_SW:
		value = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(c->widget));
		break;
	}

	return value;
}
void
control_set_value(struct control *c, unsigned int value)
{
	/* Avoid infinite callback loop */
	switch (c->type) {
	case SIOCTL_NUM:
		g_signal_handlers_block_by_func(c->widget, on_control_value_changed, NULL);
		gtk_range_set_value(GTK_RANGE(c->widget), value * 1.);
		g_signal_handlers_unblock_by_func(c->widget, on_control_value_changed, NULL);
		break;
	case SIOCTL_SW:
		g_signal_handlers_block_by_func(c->widget, on_control_value_changed, NULL);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(c->widget), value);
		g_signal_handlers_unblock_by_func(c->widget, on_control_value_changed, NULL);
		break;
	}
}

struct slice *
slice_new(char *name)
{
	struct slice *s;
	GtkWidget *label;

	s = malloc(sizeof(struct slice));
	s->group = NULL;
	s->sibling = NULL;
	s->control = NULL;

	label = gtk_label_new(name);
	strlcpy(s->name, name, STR_MAXSIZE); /* Reuse gtk_label value instead? */
	s->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_box_pack_end(GTK_BOX(s->vbox), label, 0, 0, 0);

	return s;
}

void
slice_free(struct slice *s)
{
	gtk_widget_destroy(s->vbox);
	free(s);
}

void
slice_attach_control(struct slice *s, struct control *c) {

	c->slice = s;
	c->sibling = s->control;
	s->control = c;
}

void
slice_detach_control(struct slice *s, struct control *c) {
	struct control **p;

	for (p = &s->control; *p != NULL; p = &(*p)->sibling)
		if (*p == c) {
			c->slice = NULL;
			*p = c->sibling;
			return;
		}

	if (*p == NULL)
		printf("Control could not be found in slice\n");
}

struct group *
group_new(char *name)
{
	struct group *g;
	GtkWidget *label;

	g = malloc(sizeof(struct group));
	g->sibling = NULL;
	g->slice = NULL;

	strlcpy(g->name, name, STR_MAXSIZE);
	g->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	g->hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	label = gtk_label_new(name);
	gtk_box_pack_start(GTK_BOX(g->vbox), g->hbox, 1, 1, 0);
	gtk_box_pack_start(GTK_BOX(g->vbox), label, 0, 0, 0);

	return g;
}

void
group_free(struct group *g)
{
	gtk_widget_destroy(g->vbox);
	free(g);
}

void
group_attach_slice(struct group *g, struct slice *s) {

	s->group = g;
	s->sibling = g->slice;
	g->slice = s;
}

void
group_detach_slice(struct group *g, struct slice *s) {
	struct slice **p;

	for (p = &g->slice; *p != NULL; p = &(*p)->sibling)
		if (*p == s) {
			s->group = NULL;
			*p = s->sibling;
			return;
		}

	if (*p == NULL)
		printf("Slice could not be found in group\n");
}

/* Callbacks */
void
on_control_value_changed(GtkWidget *w, gpointer p)
{
	struct control *c = (struct control *) p;
	unsigned int value;

	value = control_get_value(c);
	sioctl_setval(hdl, c->addr, value);
}

/* GUI */
int
gui_build(int argc, char **argv, struct gui *g)
{
	GtkWidget      *window;
	GtkWidget      *scrolled_window;
	GtkWidget      *box;

	gtk_init(&argc, &argv);

	/* Init */
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "GUI sndioctl");
	gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);
	scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_set_homogeneous(GTK_BOX(box), TRUE);
	g->window = window;
	g->box = box;

	/* Packing */
	gtk_container_add(GTK_CONTAINER(window), scrolled_window);
	gtk_container_add(GTK_CONTAINER(scrolled_window), box);

	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), g);
	gtk_widget_show_all(window);

	return 0;
}

void
gui_attach_group(struct gui *gui, struct group *g) {

	g->sibling = gui->group;
	gui->group = g;
}

void
gui_detach_group(struct gui *gui, struct group *g) {
	struct group **p;

	for (p = &gui->group; *p != NULL; p = &(*p)->sibling)
		if (*p == g) {
			*p = g->sibling;
			return;
		}

	if (*p == NULL)
		printf("Group could not be found in gui\n");
}

/*
 * Add/delete control, called from the poll() loop
 */
void
ondesc(void *arg, struct sioctl_desc *d, int curval)
{
	struct gui *gui = arg;
	struct control *next, **pc;
	struct slice *s;
	struct group *g;
	char name[STR_MAXSIZE], unit[4];
	unsigned int use_short_name, can_widget_expand;


	if (d == NULL)
		return;

	next = NULL;
	/* Look for control to be replaced */
	for (pc = &gui->control; *pc != NULL; pc = &(*pc)->next) {
		if (d->addr == (*pc)->addr) {
			next = (*pc)->next;
			s = (*pc)->slice;
			slice_detach_control(s, *pc);
			control_free(*pc);
			/* Remove empty layouts */
			if (s->control == NULL) {
				g = s->group;
				group_detach_slice(g, s);
				slice_free(s);
				if (g->slice == NULL) {
					gui_detach_group(gui, g);
					group_free(g);
				}
			}
			break;
		}
	}

	/* Find or create layout */
	/* Group */
	if (strlen(d->group) == 0) {
		strlcpy(name, d->node0.name, STR_MAXSIZE);
		use_short_name = TRUE;
	}
	else {
		strlcpy(name, d->group, STR_MAXSIZE);
		use_short_name = FALSE;
	}

	for (g = gui->group; g != NULL; g = g->sibling)
		if (strcmp(g->name, name) == 0)
			break;
	if (g == NULL) {
		g = group_new(name);
		gui_attach_group(gui, g);
		gtk_box_pack_start(GTK_BOX(gui->box), g->vbox, 1, 1, 0);
	}

	/* Slice */
	if (d->node0.unit >= 0) /* Useful for i.e. input/output stereo */
		snprintf(unit, 4, "%d", d->node0.unit);
	else
		snprintf(unit, 4, "");
	/* Use only unit as name if the group already uses the name */
	if (use_short_name && strlen(unit) > 0)
		strlcpy(name, unit, STR_MAXSIZE);
	else {
		strlcpy(name, d->node0.name, STR_MAXSIZE);
		strlcat(name, unit, STR_MAXSIZE);
	}

	for (s = g->slice; s!= NULL; s = s->sibling) {
		if (strcmp(s->name, name) == 0) {
			break;
		}
	}
	if (s == NULL) {
		s = slice_new(name);
		group_attach_slice(g, s);
		gtk_box_pack_start(GTK_BOX(g->hbox), s->vbox, 1, 1, 0);
	}

	/* Control */
	*pc = control_new(d);
	slice_attach_control(s, *pc);
	(*pc)->next = next;
	control_set_value(*pc, curval);
	can_widget_expand = TRUE;
	if ((*pc)->type == SIOCTL_SW)
		can_widget_expand = FALSE;
	gtk_box_pack_end(GTK_BOX(s->vbox), (*pc)->widget, can_widget_expand, 1, 0);
	gtk_widget_show_all(gui->box);
}

/*
 * update a knob/button state, called from the poll() loop
 */
void
onval(void *arg, unsigned addr, unsigned val)
{
	struct gui *gui = (struct gui *) arg;
	struct control *c;

	for (c = gui->control; c != NULL; c = c->next) {
		if (c->addr == addr) {
			break;
		}
	}
	if (c == NULL) {
		printf("Control %d was not found\n", addr);
		return;
	}

	control_set_value(c, val);
}

/*
 * Call poll(2), for both gtk and sndio descriptors.
 */
int
gpoll(GPollFD *gtk_pfds, guint gtk_nfds, gint timeout)
{
#define MAXFDS 64
	struct pollfd pfds[MAXFDS], *sioctl_pfds;
	int sioctl_nfds;
	int revents;
	int rc;
	int i;

	for (i = 0; i < gtk_nfds; i++) {
		pfds[i].fd = gtk_pfds[i].fd;
		pfds[i].events = gtk_pfds[i].events;
	}

	if (hdl != NULL) {
		sioctl_pfds = pfds + gtk_nfds;
		sioctl_nfds = sioctl_pollfd(hdl, sioctl_pfds, POLLIN);
	} else
		sioctl_nfds = 0;

	rc = poll(pfds, gtk_nfds + sioctl_nfds, timeout);

	if (rc > 0 && hdl != NULL) {
		revents = sioctl_revents(hdl, sioctl_pfds);
		if (revents & POLLHUP) {
			fprintf(stderr, "device disconnected\n");
			exit(1); /* XXX: close window ? */
		}
	}

	for (i = 0; i < gtk_nfds; i++)
		gtk_pfds[i].revents = pfds[i].revents;

	return rc;
}

int
main(int argc, char **argv)
{
	char *devname = SIO_DEVANY;
	struct gui *gui;

	gui = malloc(sizeof(struct gui));
	gui->control = NULL;
	gui->group = NULL;
	gui_build(argc, argv, gui);

	hdl = sioctl_open(devname, SIOCTL_READ | SIOCTL_WRITE, 0);
	if (hdl == NULL) {
		fprintf(stderr, "%s: can't open mixer device\n", devname);
		exit(1);
	}
	if (!sioctl_ondesc(hdl, ondesc, gui)) {
		fprintf(stderr, "%s: can't get mixer description\n", devname);
		exit(1);
	}
	sioctl_onval(hdl, onval, gui);

	g_main_context_set_poll_func(g_main_context_default(), gpoll);
	gtk_main();

	sioctl_close(hdl);
	return 0;
}
