# Guisndioctl

Guisndioctl is a GTK+ frontend to OpenBSD's sndioctl.
The goal is to reproduce the functionality exposed by sndioctl. So far volume control. In a near future, it should be possible to feed audio streams between programs, much like JACK.

# Build

Dependency is gtk+3

To build:
```
$ make
```
