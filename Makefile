CFLAGS = -g -Wall
INCLUDE = -I/usr/include
LIB = -L/usr/lib
LDADD = -lsndio
GTK_INCLUDE = `pkg-config --cflags gtk+-3.0`
GTK_LDADD = `pkg-config --libs gtk+-3.0`

OBJS = guisndioctl.o

all:		guisndioctl

clean:
		rm -f *.o
		rm -f guisndioctl

guisndioctl:	${OBJS}
		${CC} ${LDFLAGS} ${LIB} ${LDADD} ${GTK_LDADD} ${OBJS} -o guisndioctl
.c.o:
		${CC} ${CFLAGS} ${INCLUDE} ${GTK_INCLUDE} -c $<

guisndioctl.o: 	guisndioctl.c
